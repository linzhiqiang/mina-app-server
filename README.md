#mina-app-server

mina-app-server 是一个 Java 的远程调用框架，基于 
[Apache Mina](http://mina.apache.org/)。它主要用于开发独立的远程调用的服务。

详细信息请参考 [wiki](http://git.oschina.net/yidinghe/mina-app-server/wikis/home)。
